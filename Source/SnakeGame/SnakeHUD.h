// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/HUD.h"
#include "SnakeHUD.generated.h"

UCLASS()
class SNAKEGAME_API ASnakeHUD : public AHUD
{
	GENERATED_BODY()

public:
	//UPROPERTY(EditDefaultsOnly)
	//APlayerController* PlayerOwner;

	UPROPERTY(EditDefaultsOnly)
	FString PlayerScore;


	UPROPERTY()
	FString ViewportSizeHUD;
	UPROPERTY()
	FString ViewportIsValid;
	UPROPERTY()
	FString OrthoWidthBefore;
	UPROPERTY()
	FString OrthoWidthAfter;


	UPROPERTY(EditDefaultsOnly)
	UFont* PlayerScoreFont;
	
	UFUNCTION()
	void DrawHUD();

};

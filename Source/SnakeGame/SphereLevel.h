// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "SphereLevel.generated.h"

class APlayerPawnBase;

UCLASS()
class SNAKEGAME_API ASphereLevel : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ASphereLevel();
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
	UStaticMeshComponent* SphereMeshComponent;
	UPROPERTY(EditAnywhere)
	FVector SphereBoundsMin;
	UPROPERTY(EditAnywhere)
	FVector SphereBoundsMax;
	UPROPERTY()
	float SphereRadius;
	UPROPERTY()
	APlayerPawnBase* PawnOwner;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;


};

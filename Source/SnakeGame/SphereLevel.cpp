// Fill out your copyright notice in the Description page of Project Settings.


#include "SphereLevel.h"
#include "PlayerPawnBase.h"

// Sets default values
ASphereLevel::ASphereLevel()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	SphereMeshComponent = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("MeshComponent"));
}

// Called when the game starts or when spawned
void ASphereLevel::BeginPlay()
{
	Super::BeginPlay();
	SphereMeshComponent->GetLocalBounds(SphereBoundsMin, SphereBoundsMax);
	SphereRadius = (SphereBoundsMax.X - SphereBoundsMin.X) / 2;

}

// Called every frame
void ASphereLevel::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}


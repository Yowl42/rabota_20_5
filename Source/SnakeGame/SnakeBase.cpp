// Fill out your copyright notice in the Description page of Project Settings.


#include "SnakeBase.h"
#include "SnakeElementBase.h"
#include "PlayerPawnBase.h"
#include "SphereLevel.h"
#include "Interactable.h"
#include "SnakeHUD.h"
#include "Kismet/GameplayStatics.h"

// Sets default values
ASnakeBase::ASnakeBase()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	StartingSnakeElements = 5;
	ElementSize = 100.f;
	MovementSpeed = 10.f;
	SphereStepAngle = 3;
	SnakeMoveRadius = 0;
	GridHalf = 7;
	SnakeSpawnRotation = FRotator(0, 0, 0);
	HeadOrientationF = FVector(ForceInitToZero);
	HeadOrientationR = FVector(ForceInitToZero);
	SnakeRequestedMovementDirection = EMovementDirection::UP;
	ProceduralGrid = CreateDefaultSubobject<UProceduralMeshComponent>(TEXT("Procedural Grid"));
	RootComponent = ProceduralGrid;
	SnakeQuat = FQuat(0, 0, 0, 1);
}

// Called when the game starts or when spawned
void ASnakeBase::BeginPlay()
{
	Super::BeginPlay();
	if (!SaveGameInstance)
	{
		SaveGameInstance = Cast<USnakeGameScoreSave>(UGameplayStatics::CreateSaveGameObject(USnakeGameScoreSave::StaticClass()));
	}
	ProceduralGrid->OnComponentBeginOverlap.AddDynamic(this, &ASnakeBase::HandleBeginGridOverlap);
	//ProceduralGrid->SetCollisionEnabled(ECollisionEnabled::QueryOnly);
	//ProceduralGrid->SetCollisionResponseToAllChannels(ECR_Overlap);
	SetActorTickInterval(MovementSpeed);

}

// Called every frame
void ASnakeBase::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	if (IsValid (PawnOwner) && (SnakeElements.Num() < StartingSnakeElements))
	{
		SnakeMoveRadius = (PawnOwner->SphereLevelActor->SphereRadius) * 25 + ElementSize/2;
		SnakeSpawnLocation = FVector(0, 0, SnakeMoveRadius);
		AddSnakeElement(StartingSnakeElements);
		AttachToActor(SnakeElements[0], FAttachmentTransformRules::KeepWorldTransform);
		StartingPositionForGrid = FVector(0, 0, (PawnOwner->SphereLevelActor->SphereRadius) * 25);
		CalculateGrid(GridHalf, StartingPositionForGrid, SphereStepAngle, GridPoints, HeadOrientationR, ProceduralMeshVertices, Normals, TrianglesArray, Colors, VectorsUV, Tangents);
	}
	Move();
	MoveGridActors();
	UpdateHeadOrientationVectors();
	CameraUp = GetCameraUp();
	CameraRight = GetCameraRight();
	TickEnded = true;
}

FVector ASnakeBase::GetMovementVector()
{
	FVector	MovementVector(ForceInitToZero);
	
	switch (SnakeRequestedMovementDirection)
	{
	case EMovementDirection::UP:
		MovementVector = CameraUp;
		break;
	case EMovementDirection::DOWN:
		MovementVector = -CameraUp;
		break;
	case EMovementDirection::LEFT:
		MovementVector = -CameraRight;
		break;
	case EMovementDirection::RIGHT:
		MovementVector = CameraRight;
		break;

	}
	return MovementVector;
}

void ASnakeBase::AddSnakeElement(int ElementsNum)
{
	for (int i = 0; i < ElementsNum; ++i)
	{
		//����������, ���� �������� ����� �������

		FVector NewSpawnLocation = SnakeSpawnLocation;
		FRotator NewSpawnRotation = SnakeSpawnRotation;
		if ((SnakeElements.Num() > 0))
		{
			FVector LocationOfLastElement = SnakeElements[SnakeElements.Num() - 1]->GetActorLocation();
			FVector DirectionOfLastElement = SnakeElements[SnakeElements.Num() - 1]->GetActorForwardVector();
			FQuat StepQuatForLocation = GetSphereStepQuat(LocationOfLastElement, -DirectionOfLastElement, SphereStepAngle);
			FQuat StepQuatForRotation = GetSphereStepQuat(DirectionOfLastElement, LocationOfLastElement, SphereStepAngle);
			NewSpawnLocation = StepQuatForLocation.RotateVector(LocationOfLastElement);
			NewSpawnRotation = StepQuatForRotation.RotateVector(DirectionOfLastElement).Rotation();
		}
		
		FTransform NewTransform(NewSpawnRotation, NewSpawnLocation);
		ASnakeElementBase* NewSnakeElem = GetWorld()->SpawnActor<ASnakeElementBase>(SnakeElementClass, NewTransform);
		NewSnakeElem->SnakeOwner = this;
		int32 ElemIndex = SnakeElements.Add(NewSnakeElem);
		if (ElemIndex == 0)
		{
			NewSnakeElem->SetFirstElementType();
			CameraLocation = NewSnakeElem->GetActorLocation();
			UpdateHeadOrientationVectors();
			CameraUp = HeadOrientationF;
			CameraRight = HeadOrientationR;
		}
	}
}

void ASnakeBase::Move()
{
	//	PawnOwner->MainCameraHUD->ViewportIsValid = HeadLocation.ToString();
	//	FRotator NewRotation(GetMovementVector().Rotation());
	if (SnakeElements.Num() > 0)
	{
		FVector SnakeHeadLocation = SnakeElements[0]->GetActorLocation();
		FVector MovementVector = GetMovementVector();
		FQuat NewElementLocationQuat = GetSphereStepQuat(SnakeHeadLocation, MovementVector, SphereStepAngle);
		FQuat NewForwardVectorQuat = GetSphereStepQuat(MovementVector, -SnakeHeadLocation, SphereStepAngle);
		FVector NewElementLocation = NewElementLocationQuat.RotateVector(SnakeHeadLocation);
		FVector NewForwardVector = NewForwardVectorQuat.RotateVector(MovementVector);
		FRotator NewElementRotation = FRotationMatrix::MakeFromXZ(NewForwardVector.GetSafeNormal(), NewElementLocation.GetSafeNormal()).Rotator();
		//SnakeQuat *= NewElementLocationQuat;
		SnakeQuat = NewElementLocationQuat * SnakeQuat;
		for (int i = 0; i < SnakeElements.Num(); i++)
		{
			FVector PrevLocation = SnakeElements[i]->GetActorLocation();
			FRotator PrevRotation = SnakeElements[i]->GetActorRotation();
			SnakeElements[i]->SetActorLocation(NewElementLocation);
			SnakeElements[i]->SetActorRotation(NewElementRotation);
			NewElementLocation = PrevLocation;
			NewElementRotation = PrevRotation;
		}
	}
}

void ASnakeBase::SnakeElementOverlap(ASnakeElementBase* OverlappedElement, AActor* Other)
{
	if (IsValid(OverlappedElement))
	{
		int32 ElemIndex;
		SnakeElements.Find(OverlappedElement, ElemIndex);
		bool bIsFirst = ElemIndex == 0;
		IInteractable* InteractableInterface = Cast<IInteractable>(Other);
		if (InteractableInterface)
		{
			InteractableInterface->Interact(this, bIsFirst);
		}
	}
}

void ASnakeBase::PlayerDestroy()
{
	for (int i = 0; i < SnakeElements.Num(); i++)
	{
		SnakeElements[i]->Destroy();
	}
	//Save to slot
	bool bSavedGameExists = UGameplayStatics::DoesSaveGameExist(TEXT("RecordScore"), 0);
	if (bSavedGameExists)
	{
		USnakeGameScoreSave* LoadedGame = Cast<USnakeGameScoreSave>(UGameplayStatics::LoadGameFromSlot(TEXT("RecordScore"), 0));
		int32 RecordScore = LoadedGame->PlayerRecordScore;
		if (PlayerCurrentScore > RecordScore)
		{
			SaveGameInstance->PlayerRecordScore = PlayerCurrentScore;
			UGameplayStatics::SaveGameToSlot(SaveGameInstance, TEXT("RecordScore"), 0);
		}

	}
	else
	{
		UGameplayStatics::SaveGameToSlot(SaveGameInstance, TEXT("RecordScore"), 0);
	}
	GetWorld()->GetGameViewport()->RemoveAllViewportWidgets();
	UGameplayStatics::OpenLevel(GetWorld(), TEXT("Menu"));
	Destroy();
}

void ASnakeBase::UpdateHeadOrientationVectors()
{
	if (SnakeElements.Num()>0)
	{
		HeadOrientationF = SnakeElements[0]->GetActorForwardVector();
		HeadOrientationR = SnakeElements[0]->GetActorRightVector();
	}
}

FVector ASnakeBase::GetCameraUp()
{
	
	FVector NewCameraDirection(ForceInitToZero);
	if (SnakeElements.Num() > 0)
	{
		FVector CheckVector(ForceInitToZero);
		float MaxDotProduct = 0;

		for (int i = -1; i < 2; i++)
		{
			for (int j = -1; j < 2; j++)
			{
				if (XOR((i == 0), (j == 0)))
				{
					CheckVector = HeadOrientationF * i + HeadOrientationR * j;
					if (FVector::DotProduct(CameraUp, CheckVector) > MaxDotProduct)
					{
						MaxDotProduct = FVector::DotProduct(CameraUp, CheckVector);
						NewCameraDirection = CheckVector;
					}
				}
			}
		}
	}
	return NewCameraDirection;
}

FVector ASnakeBase::GetCameraRight()
{
	FVector Vector;
	if (SnakeElements.Num() > 0)
	{
		Vector = FVector::CrossProduct(CameraUp, SnakeElements[0]->GetActorLocation()).GetSafeNormal();
	}
	return Vector;
}

FQuat ASnakeBase::GetSphereStepQuat(FVector CurrentVector, FVector StepDirection, float Angle)
{
	FVector CrossProduct = FVector::CrossProduct(CurrentVector, StepDirection).GetSafeNormal();
	FQuat Quat = FQuat(CrossProduct, FMath::DegreesToRadians(Angle));
	//FVector SphereStepVector = Quat.RotateVector(CurrentVector);
	//FVector SphereStepVector = CurrentVector.RotateAngleAxis(Angle, CrossProduct);
	return Quat;
}

void ASnakeBase::CalculateGrid(int16 AmountOfPointsFromCenter,
							   FVector StartingVector,
							   float StepAngle,
							   TArray<FGridPointStruct>& ArrayForPoints,
							   FVector RightVector,
							   TArray<FVector>& MeshVert,
							   TArray<FVector>& Norm,
							   TArray<int>& TriangInd,
							   TArray<FLinearColor>& Clrs,
							   TArray<FVector2D>& UV,
							   TArray<FProcMeshTangent>& Tang)
{
	FVector CurrentVector = StartingVector;
	FVector AxisV = RightVector;
	FVector AxisH = FVector::CrossProduct(CurrentVector, AxisV).GetSafeNormal();
	float GridCoefficient = 0.2;
	float GridWidthAngle = StepAngle * GridCoefficient;
	int16 NumberOfElements = (AmountOfPointsFromCenter * 2 + 1)*(AmountOfPointsFromCenter * 2 + 1);
	int8 Alpha;

	//���������� ��� �������� UV �����
	
	//float UVStep1 = GridWidthAngle / ((NumberOfElements - 1) * StepAngle + GridWidthAngle);
	//float UVStep2 = (1 - UVStep1 * NumberOfElements) / (NumberOfElements - 1);
	float UVStep1 = GridCoefficient / (2 * AmountOfPointsFromCenter + GridCoefficient);
	float UVStep2 = (1-GridCoefficient)*(1-UVStep1)/(2*AmountOfPointsFromCenter);
	float ProgressU = 0;
	float ProgressV = 1;

	


	for (int i = -AmountOfPointsFromCenter; i <= AmountOfPointsFromCenter; i++)
	{
		
		CurrentVector = CurrentVector.RotateAngleAxis(StepAngle * i, AxisV);
		AxisH = AxisH.RotateAngleAxis(StepAngle * i, AxisV);
		//		ProgressV = (i + AmountOfPointsFromCenter) * (UVStep1 + UVStep2);
		for (int j = -AmountOfPointsFromCenter; j <= AmountOfPointsFromCenter; j++)
		{
			
			FGridPointStruct NewPoint;
			NewPoint.GridPointLocalPosition = CurrentVector.RotateAngleAxis(StepAngle * j, AxisH);
			NewPoint.GridPointRVector = AxisV.RotateAngleAxis(StepAngle * j, AxisH);
			NewPoint.bEdgePoint = (FMath::Abs(i) == AmountOfPointsFromCenter) || (FMath::Abs(j) == AmountOfPointsFromCenter);
			NewPoint.bEndl = (j == AmountOfPointsFromCenter);
			NewPoint.bEndc = (i == AmountOfPointsFromCenter);
			NewPoint.bStartl = (j == -AmountOfPointsFromCenter);
			NewPoint.bStartc = (i == -AmountOfPointsFromCenter);
			//if (NewPoint.bEdgePoint)
			//{
			CollisionVertices.Add(SnakeMoveRadius * NewPoint.GridPointLocalPosition.GetSafeNormal());
			//}
			//			ProgressU = (j + AmountOfPointsFromCenter) * (UVStep1 + UVStep2);
			FVector CurrentVector2 = NewPoint.GridPointLocalPosition;
			FVector AxisV2 = NewPoint.GridPointRVector;
			FVector AxisH2 = FVector::CrossProduct(CurrentVector2, AxisV2).GetSafeNormal();

			for (int k = -1; k <= 1; k += 2)
			{
				CurrentVector2 = CurrentVector2.RotateAngleAxis(GridWidthAngle / 2 * k, AxisV2);
				AxisH2 = AxisH2.RotateAngleAxis(GridWidthAngle / 2 * k, AxisV2);
				for (int l = -1; l <= 1; l += 2)
				{
					MeshVert.Add(CurrentVector2.RotateAngleAxis(GridWidthAngle / 2 * l, AxisH2));
					Norm.Add(CurrentVector2.RotateAngleAxis(GridWidthAngle / 2 * l, AxisH2).GetSafeNormal());
					float CoordinateU = ProgressU + UVStep1 * (l + 1) / 2;
					float CoordinateV = ProgressV - UVStep1 * (k + 1) / 2;
					float d = FMath::Sqrt(FMath::Square(CoordinateU - 0.5) + FMath::Square((CoordinateV - 0.5)));
					Alpha = FMath::Max(0, FMath::RoundToInt(255 * (1 - d / 0.5)));
					Clrs.Add(FLinearColor(255, 255, 255, Alpha));
					UV.Add(FVector2D(FMath::Min(1.f, CoordinateU), FMath::Min(1.f, CoordinateV)));
					Tang.Add(FProcMeshTangent(AxisV2.RotateAngleAxis(GridWidthAngle / 2 * l, AxisH2), false));
				}
				CurrentVector2 = NewPoint.GridPointLocalPosition;
				AxisH2 = FVector::CrossProduct(CurrentVector2, AxisV2).GetSafeNormal();
			}
			ArrayForPoints.Add(NewPoint);
			ProgressU += (UVStep1 + UVStep2);
			
		}
	
	CurrentVector = StartingVector;
	AxisH = FVector::CrossProduct(StartingVector, RightVector).GetSafeNormal();
	ProgressU = 0;
	ProgressV -= (UVStep1 + UVStep2);
	}

	//���� ��� ���������� �������� ������ �������������
	for (int i = 0; i < (NumberOfElements * 4); i += 4)
	{
		TriangInd.Append({ i, i + 1, i + 2, i + 2, i + 1, i + 3});
		if (!ArrayForPoints[FMath::RoundToInt(i / 4)].bEndl)
		{
			TriangInd.Append({ i + 3, i + 1, i + 4, i + 3, i + 4, i + 6});
		}
		int Side = FMath::RoundToInt(FMath::Sqrt(NumberOfElements))*4;
		if (!ArrayForPoints[FMath::RoundToInt(i / 4)].bEndc)
		{
			TriangInd.Append({ i + 2,i + 3,i + Side,i + Side,i + 3,i + Side + 1});
		}
	}
	bGridParametersAreSet = true;
	//ProceduralGrid->CreateMeshSection(0, ProceduralMeshVertexes, TrianglesArray, Normals, VectorsUV, Colors, Tangents, false);
}

void ASnakeBase::HandleBeginGridOverlap(UPrimitiveComponent* OverlappedComponent, 
	AActor* OtherActor, 
	UPrimitiveComponent* OtherComp, 
	int32 OtherBodyIndex, 
	bool bFromSweep, 
	const FHitResult& SweepResult)
{
	auto ActorCheck = Cast<IInteractable>(OtherActor);
	bool bActorOnGridCheck;
	bActorOnGridCheck =	FindActorOnGrid(OtherActor);
	GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Red, FString::Printf(TEXT("%p"), OtherActor));
	for (int i = 0; i < ActorsOnGrid.Num(); i++)
	{
		GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Green, FString::Printf(TEXT("%p"), ActorsOnGrid[i].AcrorRef));
	}
	if ((ActorCheck) && (!bActorOnGridCheck))
	{
		SnapToGrid(OtherActor);
	}
}

void ASnakeBase::SnapToGrid(AActor* Other)
{
	//Other->FindComponentByClass<UMeshComponent>()->SetCollisionResponseToChannel(CollsionChannelToIgnore, ECollisionResponse::ECR_Ignore);
	IInteractable* OtherActor = Cast<IInteractable>(Other);
	if (OtherActor)
	{
		OtherActor->SetGridCollisionIsActive(false);
		
		float MinDistance = 2000;
		int index = 0;
		FVector WorldPointPosition(ForceInitToZero);
		FVector WorldFinalPointPosition(ForceInitToZero);
		FRotator FinalRotator(ForceInitToZero);
		float CurrentDistance = 0;
		FTransform SnakeHeadTransform(SnakeQuat);
		//SnakeHeadTransform.SetLocation(FVector(0, 0, 0));
		for (int i = 0; i < GridPoints.Num(); i++)
		{
			//GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Orange, TEXT("Messege"));
			if (GridPoints[i].bEdgePoint)
			{
				WorldPointPosition = SnakeHeadTransform.TransformPosition(GridPoints[i].GridPointLocalPosition);

				CurrentDistance = FVector::Dist(WorldPointPosition, Other->GetActorLocation());
				if (CurrentDistance < MinDistance)
				{
					//GEngine->AddOnScreenDebugMessage(-1, 600.f, FColor::Orange, FString::Printf(TEXT("Min Distance %f"), MinDistance));
					MinDistance = CurrentDistance;
					index = i;
					WorldFinalPointPosition = WorldPointPosition;

				}
			}
		}
		//GetWorld()->GetFirstPlayerController()->SetPause(true);
		Other->SetActorLocation(WorldFinalPointPosition.GetSafeNormal()*SnakeMoveRadius);
		FinalRotator = FRotationMatrix::MakeFromXY(WorldFinalPointPosition, GridPoints[index].GridPointRVector).Rotator();
		Other->SetActorRotation(FinalRotator);
		FActorOnGrid NewGridActor;
		NewGridActor.AcrorRef = Other;
		NewGridActor.GridPointIndex = index;
		ActorsOnGrid.Add(NewGridActor);
		GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Yellow, TEXT("Actor Added to grid"));
	}
}

void ASnakeBase::MoveGridActors()
{
	if (ActorsOnGrid.Num() > 0)
	{
		for (int i = 0; i < ActorsOnGrid.Num(); i++)
		{
			IInteractable* OtherActor = Cast<IInteractable>(ActorsOnGrid[i].AcrorRef);
			FVector NewActorLocation(ForceInitToZero);
			FRotator NewActorRotator(ForceInitToZero);
			FVector Axis(ForceInitToZero);
			FTransform SnakeHeadTransform(SnakeQuat);
			//SnakeHeadTransform.SetLocation(FVector(0, 0, 0));
			if (SnakeRequestedMovementDirection == EMovementDirection::LEFT)
			{
				if (GridPoints[ActorsOnGrid[i].GridPointIndex].bStartl)
				{
					Axis = SnakeHeadTransform.TransformPosition(GridPoints[ActorsOnGrid[i].GridPointIndex].GridPointRVector);
					NewActorLocation = SnakeHeadTransform.TransformPosition(GridPoints[ActorsOnGrid[i].GridPointIndex].GridPointLocalPosition);
					Axis = FVector::CrossProduct(NewActorLocation, Axis).GetSafeNormal();
					NewActorLocation = NewActorLocation.RotateAngleAxis(-SphereStepAngle, Axis);
					ActorsOnGrid[i].AcrorRef->SetActorLocation(NewActorLocation.GetSafeNormal() * SnakeMoveRadius);
					NewActorRotator = FRotationMatrix::MakeFromXY(NewActorLocation, GridPoints[ActorsOnGrid[i].GridPointIndex].GridPointRVector).Rotator();
					ActorsOnGrid[i].AcrorRef->SetActorRotation(NewActorRotator);
					//ActorsOnGrid[i].AcrorRef->FindComponentByClass<UMeshComponent>()->SetCollisionResponseToChannel(CollsionChannelToIgnore, ECollisionResponse::ECR_Overlap);
					OtherActor->SetGridCollisionIsActive(true);
					RemoveFromGrid(ActorsOnGrid[i].AcrorRef);
					//ActorsOnGrid.RemoveAt(i);
				}
				else
				{
					NewActorLocation = SnakeHeadTransform.TransformPosition(GridPoints[ActorsOnGrid[i].GridPointIndex - 1].GridPointLocalPosition);
					ActorsOnGrid[i].AcrorRef->SetActorLocation(NewActorLocation.GetSafeNormal() * SnakeMoveRadius);
					NewActorRotator = FRotationMatrix::MakeFromXY(NewActorLocation, GridPoints[ActorsOnGrid[i].GridPointIndex].GridPointRVector).Rotator();
					ActorsOnGrid[i].AcrorRef->SetActorRotation(NewActorRotator);
					ActorsOnGrid[i].GridPointIndex = ActorsOnGrid[i].GridPointIndex - 1;
				}
			}
			else if (SnakeRequestedMovementDirection == EMovementDirection::RIGHT)
			{
				if (GridPoints[ActorsOnGrid[i].GridPointIndex].bEndl)
				{
					Axis = SnakeHeadTransform.TransformPosition(GridPoints[ActorsOnGrid[i].GridPointIndex].GridPointRVector);
					NewActorLocation = SnakeHeadTransform.TransformPosition(GridPoints[ActorsOnGrid[i].GridPointIndex].GridPointLocalPosition);
					Axis = FVector::CrossProduct(NewActorLocation, Axis).GetSafeNormal();
					NewActorLocation = NewActorLocation.RotateAngleAxis(SphereStepAngle, Axis);
					ActorsOnGrid[i].AcrorRef->SetActorLocation(NewActorLocation.GetSafeNormal() * SnakeMoveRadius);
					NewActorRotator = FRotationMatrix::MakeFromXY(NewActorLocation, GridPoints[ActorsOnGrid[i].GridPointIndex].GridPointRVector).Rotator();
					ActorsOnGrid[i].AcrorRef->SetActorRotation(NewActorRotator);
					//ActorsOnGrid[i].AcrorRef->FindComponentByClass<UMeshComponent>()->SetCollisionResponseToChannel(CollsionChannelToIgnore, ECollisionResponse::ECR_Overlap);
					OtherActor->SetGridCollisionIsActive(true);
					RemoveFromGrid(ActorsOnGrid[i].AcrorRef);
					//ActorsOnGrid.RemoveAt(i);
				}
				else
				{
					NewActorLocation = SnakeHeadTransform.TransformPosition(GridPoints[ActorsOnGrid[i].GridPointIndex + 1].GridPointLocalPosition);
					ActorsOnGrid[i].AcrorRef->SetActorLocation(NewActorLocation.GetSafeNormal() * SnakeMoveRadius);
					NewActorRotator = FRotationMatrix::MakeFromXY(NewActorLocation, GridPoints[ActorsOnGrid[i].GridPointIndex].GridPointRVector).Rotator();
					ActorsOnGrid[i].AcrorRef->SetActorRotation(NewActorRotator);
					ActorsOnGrid[i].GridPointIndex = ActorsOnGrid[i].GridPointIndex + 1;
				}
			}
			else if (SnakeRequestedMovementDirection == EMovementDirection::UP)
			{
				if (GridPoints[ActorsOnGrid[i].GridPointIndex].bStartc)
				{
					Axis = SnakeHeadTransform.TransformPosition(GridPoints[ActorsOnGrid[i].GridPointIndex].GridPointRVector);
					NewActorLocation = SnakeHeadTransform.TransformPosition(GridPoints[ActorsOnGrid[i].GridPointIndex].GridPointLocalPosition);
					NewActorLocation = NewActorLocation.RotateAngleAxis(-SphereStepAngle, Axis);
					ActorsOnGrid[i].AcrorRef->SetActorLocation(NewActorLocation.GetSafeNormal() * SnakeMoveRadius);
					NewActorRotator = FRotationMatrix::MakeFromXY(NewActorLocation, GridPoints[ActorsOnGrid[i].GridPointIndex].GridPointRVector).Rotator();
					ActorsOnGrid[i].AcrorRef->SetActorRotation(NewActorRotator);
					//ActorsOnGrid[i].AcrorRef->FindComponentByClass<UMeshComponent>()->SetCollisionResponseToChannel(CollsionChannelToIgnore, ECollisionResponse::ECR_Overlap);
					OtherActor->SetGridCollisionIsActive(true);
					RemoveFromGrid(ActorsOnGrid[i].AcrorRef);
					//ActorsOnGrid.RemoveAt(i);
				}
				else
				{
					NewActorLocation = SnakeHeadTransform.TransformPosition(GridPoints[ActorsOnGrid[i].GridPointIndex - (GridHalf * 2 + 1)].GridPointLocalPosition);
					ActorsOnGrid[i].AcrorRef->SetActorLocation(NewActorLocation.GetSafeNormal() * SnakeMoveRadius);
					NewActorRotator = FRotationMatrix::MakeFromXY(NewActorLocation, GridPoints[ActorsOnGrid[i].GridPointIndex].GridPointRVector).Rotator();
					ActorsOnGrid[i].AcrorRef->SetActorRotation(NewActorRotator);
					ActorsOnGrid[i].GridPointIndex = ActorsOnGrid[i].GridPointIndex - (GridHalf * 2 + 1);
				}
			}
			else if (SnakeRequestedMovementDirection == EMovementDirection::DOWN)
			{
				if (GridPoints[ActorsOnGrid[i].GridPointIndex].bEndc)
				{
					Axis = SnakeHeadTransform.TransformPosition(GridPoints[ActorsOnGrid[i].GridPointIndex].GridPointRVector);
					NewActorLocation = SnakeHeadTransform.TransformPosition(GridPoints[ActorsOnGrid[i].GridPointIndex].GridPointLocalPosition);
					NewActorLocation = NewActorLocation.RotateAngleAxis(SphereStepAngle, Axis);
					ActorsOnGrid[i].AcrorRef->SetActorLocation(NewActorLocation.GetSafeNormal() * SnakeMoveRadius);
					NewActorRotator = FRotationMatrix::MakeFromXY(NewActorLocation, GridPoints[ActorsOnGrid[i].GridPointIndex].GridPointRVector).Rotator();
					ActorsOnGrid[i].AcrorRef->SetActorRotation(NewActorRotator);
					//ActorsOnGrid[i].AcrorRef->FindComponentByClass<UMeshComponent>()->SetCollisionResponseToChannel(CollsionChannelToIgnore, ECollisionResponse::ECR_Overlap);
					OtherActor->SetGridCollisionIsActive(true);
					RemoveFromGrid(ActorsOnGrid[i].AcrorRef);
					//ActorsOnGrid.RemoveAt(i);
				}
				else
				{
					NewActorLocation = SnakeHeadTransform.TransformPosition(GridPoints[ActorsOnGrid[i].GridPointIndex + (GridHalf * 2 + 1)].GridPointLocalPosition);
					ActorsOnGrid[i].AcrorRef->SetActorLocation(NewActorLocation.GetSafeNormal() * SnakeMoveRadius);
					NewActorRotator = FRotationMatrix::MakeFromXY(NewActorLocation, GridPoints[ActorsOnGrid[i].GridPointIndex].GridPointRVector).Rotator();
					ActorsOnGrid[i].AcrorRef->SetActorRotation(NewActorRotator);
					ActorsOnGrid[i].GridPointIndex = ActorsOnGrid[i].GridPointIndex + (GridHalf * 2 + 1);
				}
			}
			FString Position = TEXT("ActorLocation: ") + NewActorLocation.ToString();
			GEngine->AddOnScreenDebugMessage(i, 5.f, FColor::Yellow, TEXT("Actor Added to grid"));
		}
	}
	
}

void ASnakeBase::RemoveFromGrid(AActor* Actor)
{
	for (int index = 0; index < ActorsOnGrid.Num(); index++)
	{
		if (ActorsOnGrid[index].AcrorRef == Actor)
		{
			ActorsOnGrid.RemoveAt(index);
		}
	}
}

bool ASnakeBase::FindActorOnGrid(AActor* Actor)
{
	bool bActorOnGrid = false;
	for (int index = 0; index < ActorsOnGrid.Num(); index++)
	{
		UE_LOG(LogTemp, Warning, TEXT("Comparing: %s with %s"), *ActorsOnGrid[index].AcrorRef->GetName(), *Actor->GetName());
		if (ActorsOnGrid[index].AcrorRef == Actor)
		{
			bActorOnGrid = true;
		}
	}
	return bActorOnGrid;
}

void ASnakeBase::ActivateIncreasedSpeed(float SpeedMultiplier)
{
	MovementSpeed *= SpeedMultiplier;
	GetWorld()->GetTimerManager().SetTimer(IncreasedSpeedTimer, this, &ASnakeBase::DeactivateIncreasedSpeed, 5.f, false);
}

void ASnakeBase::DeactivateIncreasedSpeed()
{
	MovementSpeed = 10.f;
}



// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Pawn.h"
#include "PlayerPawnBase.generated.h"

class UCameraComponent;
class USpringArmComponent;
class UStaticMeshComponent;
class ASnakeHUD;
class ASnakeBase;
class AFood;
class AWall;
class ASphereLevel;

UCLASS()
class SNAKEGAME_API APlayerPawnBase : public APawn
{
	GENERATED_BODY()

public:
	// Sets default values for this pawn's properties
	APlayerPawnBase();

	UPROPERTY()
	USceneComponent* BaseComponent;
	UPROPERTY(BlueprintReadWrite)
	UCameraComponent* PawnCamera;
	UPROPERTY()
	USpringArmComponent* SpringArm;
	UPROPERTY(EditDefaultsOnly)
	UStaticMeshComponent* GuidingMesh;
	UPROPERTY(BlueprintReadWrite)
	ASnakeBase* SnakeActor;
	UPROPERTY(BlueprintReadWrite)
	AFood* FoodActor;
	UPROPERTY(BlueprintReadWrite)
	TArray<AWall*> WallActors;
	UPROPERTY(BlueprintReadWrite)
	TArray<AFood*> FoodActors;
	UPROPERTY(EditDefaultsOnly)
	TSubclassOf<ASnakeBase> SnakeActorClass;
	UPROPERTY()
	UClass* FoodActorClass;
	UPROPERTY(EditDefaultsOnly)
	TSubclassOf<AWall> WallActorClass;
	UPROPERTY(EditDefaultsOnly)
	UStaticMesh* Mesh_Food;
	UPROPERTY()
	UMaterialInterface* Material_Food;
	UPROPERTY(EditDefaultsOnly)
	UMaterialInterface* Material_Food_default;
	UPROPERTY(EditDefaultsOnly)
	UMaterialInterface* Material_Food_MoreBlocks;
	UPROPERTY(EditDefaultsOnly)
	UMaterialInterface* Material_Food_Increased_Speed;
	UPROPERTY(EditDefaultsOnly)
	UMaterialInterface* Material_Grid;
	UPROPERTY()
	ASnakeHUD* MainCameraHUD;
	UPROPERTY()
	bool bCameraIsSet;
	UPROPERTY()
	ASphereLevel* SphereLevelActor;
	UPROPERTY(EditDefaultsOnly)
	TSubclassOf<ASphereLevel> SphereLevelClass;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

	void CreateSnakeActor();

	UFUNCTION()
	void HandlePlayerVerticalInput(float value);
	UFUNCTION()
	void HandlePlayerHorizontalInput(float value);
	UFUNCTION()
	void SpawnFood();
	UFUNCTION()
	void SpawnWalls(int16 Amount);
	UFUNCTION()
	void SetCameraOrthoWidth();
	UFUNCTION()
	void CreateSphereLevel();
	
	
};

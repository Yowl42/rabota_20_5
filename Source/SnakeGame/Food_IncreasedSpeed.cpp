// Fill out your copyright notice in the Description page of Project Settings.


#include "Food_IncreasedSpeed.h"
#include "PlayerPawnBase.h"
#include "SnakeBase.h"

void AFood_IncreasedSpeed::Interact(AActor* Interactor, bool bIsHead)
{
	auto Snake = Cast<ASnakeBase>(Interactor);
	if (IsValid(Snake))
	{
		Snake->AddSnakeElement(1);
		Snake->PlayerCurrentScore += 1;
		Snake->ActivateIncreasedSpeed(5);
		Snake->RemoveFromGrid(this);
		Destroy();
	}
}
// Fill out your copyright notice in the Description page of Project Settings.


#include "Food_MoreBlocks.h"
#include "SnakeHUD.h"
#include "PlayerPawnBase.h"
#include "SnakeBase.h"

void AFood_MoreBlocks::Interact(AActor* Interactor, bool bIsHead)
{
	auto Snake = Cast<ASnakeBase>(Interactor);
	if (IsValid(Snake))
	{
		Snake->AddSnakeElement(3);
		Snake->PlayerCurrentScore += 3;
		Snake->RemoveFromGrid(this);
		Destroy();
	}
}

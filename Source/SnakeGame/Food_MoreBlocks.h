// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Food.h"
#include "Food_MoreBlocks.generated.h"

/**
 * 
 */
UCLASS()
class SNAKEGAME_API AFood_MoreBlocks : public AFood
{
	GENERATED_BODY()
	
public:
	void Interact(AActor* Interactor, bool bIsHead) override;
};

// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "ProceduralMeshComponent.h"
#include "SnakeGameScoreSave.h"
#include "SnakeBase.generated.h"

class ASnakeElementBase;
class APlayerPawnBase;
class UGameplayStatics;

UENUM()
enum class EMovementDirection
{
	UP,
	DOWN,
	LEFT,
	RIGHT
};

USTRUCT(BlueprintType)
struct FGridPointStruct
{
	GENERATED_BODY()

	UPROPERTY(BlueprintReadWrite)
	FVector GridPointLocalPosition;
	UPROPERTY(BlueprintReadWrite)
	FVector GridPointRVector;
	UPROPERTY(BlueprintReadWrite)
	bool bEdgePoint;
	UPROPERTY(BlueprintReadWrite)
	bool bEndl;
	UPROPERTY(BlueprintReadWrite)
	bool bEndc;
	UPROPERTY(BlueprintReadWrite)
	bool bStartl;
	UPROPERTY(BlueprintReadWrite)
	bool bStartc;

};

USTRUCT(BlueprintType)
struct FActorOnGrid
{
	GENERATED_BODY()

	UPROPERTY()
	AActor* AcrorRef;
	UPROPERTY()
	int GridPointIndex;
};

UCLASS()
class SNAKEGAME_API ASnakeBase : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ASnakeBase();
	UPROPERTY(BlueprintReadOnly)
	TArray<FGridPointStruct> GridPoints;
	//UPROPERTY(BlueprintReadOnly)
	//TArray<FVector> GridPoints;
	//UPROPERTY(BlueprintReadOnly)
	//TArray<FVector> GridMeshPoints;
	UPROPERTY(EditDefaultsOnly)
	TSubclassOf<ASnakeElementBase> SnakeElementClass;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	UProceduralMeshComponent* ProceduralGrid;
	UPROPERTY()
	FVector StartingPositionForGrid;
	UPROPERTY(EditDefaultsOnly)
	int16 GridHalf;
	UPROPERTY(BlueprintReadOnly)
	TArray<FVector> ProceduralMeshVertices;
	UPROPERTY(BlueprintReadOnly)
	TArray<FVector> Normals;
	UPROPERTY(BlueprintReadOnly)
	TArray<int> TrianglesArray;
	UPROPERTY(BlueprintReadOnly)
	TArray<FLinearColor> Colors;
	UPROPERTY(BlueprintReadOnly)
	TArray<FVector2D> VectorsUV;
	UPROPERTY(BlueprintReadOnly)
	TArray<FProcMeshTangent> Tangents;
	UPROPERTY(BlueprintReadOnly)
	TArray<FVector> CollisionVertices;
	UPROPERTY(BlueprintReadOnly)
	TArray<FActorOnGrid> ActorsOnGrid;
	UPROPERTY(BlueprintReadOnly)
	bool bGridParametersAreSet = false;
	UPROPERTY(EditAnywhere)
	TEnumAsByte<ECollisionChannel> CollsionChannelToIgnore;
	//TArray<int16> TrianglesArray;
	UPROPERTY(EditDefaultsOnly)
	float ElementSize;
	UPROPERTY(EditDefaultsOnly)
	float MovementSpeed;
	UPROPERTY()
	TArray<ASnakeElementBase*> SnakeElements;
	UPROPERTY()
	bool TickEnded;
	UPROPERTY()
	APlayerPawnBase* PawnOwner;
	UPROPERTY()
	float SnakeMoveRadius;
	UPROPERTY()
	FVector SnakeSpawnLocation;
	UPROPERTY()
	FRotator SnakeSpawnRotation;
	UPROPERTY()
	int16 StartingSnakeElements;
	UPROPERTY(EditDefaultsOnly)
	float SphereStepAngle;

	UPROPERTY()
	EMovementDirection SnakeRequestedMovementDirection;

	UPROPERTY()
	FVector HeadOrientationF;
	UPROPERTY()
	FVector HeadOrientationR;

	UPROPERTY()
	FVector CameraLocation;
	UPROPERTY()
	FVector CameraUp;
	UPROPERTY()
	FVector CameraRight;
	UPROPERTY()
	FQuat SnakeQuat;
	UPROPERTY()
	USnakeGameScoreSave* SaveGameInstance;
	UPROPERTY(BlueprintReadOnly)
	int32 PlayerCurrentScore = 0;
	UPROPERTY()
	FTimerHandle IncreasedSpeedTimer;



protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;
	UFUNCTION()
	FVector GetMovementVector();
	UFUNCTION(BlueprintCallable)
	void AddSnakeElement(int ElementsNum = 1);
	UFUNCTION(BlueprintCallable)
	void Move();
	UFUNCTION()
	void SnakeElementOverlap(ASnakeElementBase* OverlappedElement, AActor* Other);
	UFUNCTION()
	void PlayerDestroy();
	UFUNCTION()
	void UpdateHeadOrientationVectors();
	UFUNCTION()
	FVector GetCameraUp();
//	UFUNCTION()
//	float GetAngleBetweenVectors(FVector Vector1, FVector Vector2);
	UFUNCTION()
	FVector GetCameraRight();
	UFUNCTION()
	FQuat GetSphereStepQuat(FVector CurrentLocation, FVector StepDirection, float Angle);
	UFUNCTION()
	void CalculateGrid(int16 AmountOfPointsFromCenter, 
					   FVector StartingVector, 
					   float StepAngle, 
					   TArray<FGridPointStruct> &ArrayForPoints,
					   FVector RightVector,
					   TArray<FVector> &MeshVert,
					   TArray<FVector> &Norm,
					   TArray<int> &TriangInd,
					   TArray<FLinearColor> &Clrs,
					   TArray<FVector2D> &UV,
					   TArray<FProcMeshTangent> &Tang);
	UFUNCTION()
	void HandleBeginGridOverlap(UPrimitiveComponent* OverlappedComponent,
		AActor* OtherActor,
		UPrimitiveComponent* OtherComp,
		int32 OtherBodyIndex,
		bool bFromSweep,
		const FHitResult& SweepResult);
	UFUNCTION()
	void SnapToGrid(AActor* Other);
	UFUNCTION()
	void MoveGridActors();
	UFUNCTION()
	void RemoveFromGrid(AActor* Actor);
	UFUNCTION()
	bool FindActorOnGrid(AActor* Actor);
	UFUNCTION()
	void ActivateIncreasedSpeed(float SpeedMultiplier);
	UFUNCTION()
	void DeactivateIncreasedSpeed();
};

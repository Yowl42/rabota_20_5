// Fill out your copyright notice in the Description page of Project Settings.


#include "Food.h"
#include "SnakeBase.h"
#include "SnakeHUD.h"
#include "PlayerPawnBase.h"

// Sets default values
AFood::AFood()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	MeshComponent = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("MeshComponent"));
	MeshComponent->SetCollisionProfileName(TEXT("CustomInteractable"));
	//MeshComponent->SetCollisionEnabled(ECollisionEnabled::QueryOnly);
	//MeshComponent->SetCollisionObjectType(ECollisionChannel::ECC_GameTraceChannel3);
	//MeshComponent->SetCollisionResponseToAllChannels(ECR_Overlap);
}

// Called when the game starts or when spawned
void AFood::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void AFood::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void AFood::Interact(AActor* Interactor, bool bIsHead)
{
	if (bIsHead)
	{
		auto Snake = Cast<ASnakeBase>(Interactor);
		if (IsValid(Snake))
		{
			Snake->AddSnakeElement();
			Snake->PlayerCurrentScore += 1;
			Snake->RemoveFromGrid(this);
			/*for (int i = 0; i < Snake->PawnOwner->FoodActors.Num(); i++)
			{
				UE_LOG(LogTemp, Warning, TEXT("Actor "));
			}
			for (AActor* Food : Snake->PawnOwner->FoodActors)
			{
				//UE_LOG(LogTemp, Warning, TEXT("Actor "));
				GEngine->AddOnScreenDebugMessage(-1, 600.f, FColor::Green, FString::Printf(TEXT("Actor in array %p"), Food));
			}
			GEngine->AddOnScreenDebugMessage(-1, 600.f, FColor::Red, FString::Printf(TEXT("Deleting actor %p"), this));*/
			Snake->PawnOwner->FoodActors.Remove(this);
			//for (AActor* Food : Snake->PawnOwner->FoodActors)
			//{
			//	//UE_LOG(LogTemp, Warning, TEXT("Actor "));
			//	GEngine->AddOnScreenDebugMessage(-1, 600.f, FColor::Orange, FString::Printf(TEXT("Actor in array %p"), Food));
			//}
			//GetWorld()->GetFirstPlayerController()->SetPause(true);
			Destroy();
		}
	}
}

bool AFood::GetGridCollisionIsActive() const
{
	return bGridCollisionIsActive;
}

void AFood::SetGridCollisionIsActive(bool CollisionStatus)
{
	if (CollisionStatus)
	{
		MeshComponent->SetCollisionResponseToChannel(ECollisionChannel::ECC_GameTraceChannel2, ECollisionResponse::ECR_Overlap);
	}
	else
	{
		MeshComponent->SetCollisionResponseToChannel(ECollisionChannel::ECC_GameTraceChannel2, ECollisionResponse::ECR_Ignore);
	}
}


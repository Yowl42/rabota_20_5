// Fill out your copyright notice in the Description page of Project Settings.


#include "PlayerPawnBase.h"
#include "Engine/Classes/Camera/CameraComponent.h"
#include "Engine/Classes/GameFramework/SpringArmComponent.h"
#include "Engine/Classes/Components/StaticMeshComponent.h"
//#include "GameFramework/GameUserSettings.h"
#include "SnakeHUD.h"
#include "SnakeBase.h"
#include "SnakeElementBase.h"
#include "Kismet/KismetSystemLibrary.h"
#include "Food.h"
#include "Wall.h"
#include "SphereLevel.h"
#include "Food_MoreBlocks.h"
#include "Food_IncreasedSpeed.h"
#include "Components/InputComponent.h"

// Sets default values
APlayerPawnBase::APlayerPawnBase()
{
 	// Set this pawn to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	//RootComponent = GuidingMesh;
	//SpringArm->AttachToComponent(GuidingMesh, FAttachmentTransformRules::KeepRelativeTransform);
	//PawnCamera->AttachToComponent(SpringArm,FAttachmentTransformRules::KeepRelativeTransform);

	BaseComponent = CreateDefaultSubobject<USceneComponent>(TEXT("BaseComponent"));
	RootComponent = BaseComponent;

	GuidingMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("GuidingMesh"));
	GuidingMesh->SetupAttachment(RootComponent);
	GuidingMesh->AddRelativeRotation(FRotator(-90, 0, 0));
	GuidingMesh->SetVisibility(0);
//	GuidingMesh->AddLocalRotation(FRotator(-90, 0, 0));

	SpringArm = CreateDefaultSubobject<USpringArmComponent>(TEXT("SpringArm"));
//	SpringArm->AddLocalRotation(FRotator(-90, 0, 0));
	SpringArm->SetupAttachment(GuidingMesh);
	SpringArm->TargetArmLength = 600.0f;

	PawnCamera = CreateDefaultSubobject<UCameraComponent>(TEXT("PawnCamera"));
//	PawnCamera->AddLocalRotation(FRotator(-90, 0, 0));
	PawnCamera->SetupAttachment(SpringArm, USpringArmComponent::SocketName);


//	PawnCamera->SetProjectionMode(ECameraProjectionMode::Orthographic);
	bCameraIsSet = 0;
}

// Called when the game starts or when spawned
void APlayerPawnBase::BeginPlay()
{
	Super::BeginPlay();
	MainCameraHUD = Cast<ASnakeHUD>(GetWorld()->GetFirstPlayerController()->GetHUD());
	//SetActorRotation(FRotator(-90, 0, 0));
	CreateSphereLevel();
	CreateSnakeActor();
}

// Called every frame
void APlayerPawnBase::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	/*if (!bCameraIsSet)
	{
		SetCameraOrthoWidth();
		bCameraIsSet = 1;
	}*/
	if (SnakeActor->SnakeElements.Num() > 0)
	{
		SetActorLocation(SnakeActor->SnakeElements[0]->GetActorLocation()*1.1);
		SetActorRotation(FRotationMatrix::MakeFromXY(SnakeActor->CameraUp, -SnakeActor->CameraRight).Rotator());
	}

	if ((FoodActors.Num() < 5) && SnakeActor->SnakeMoveRadius != 0)
	{
		SpawnFood();
	}

	if ((WallActors.Num() == 0) && SnakeActor->SnakeMoveRadius != 0)
	{
		SpawnWalls(5);
	}
}

// Called to bind functionality to input
void APlayerPawnBase::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);
	PlayerInputComponent->BindAxis("Vertical", this, &APlayerPawnBase::HandlePlayerVerticalInput);
	PlayerInputComponent->BindAxis("Horizontal", this, &APlayerPawnBase::HandlePlayerHorizontalInput);

}

void APlayerPawnBase::CreateSnakeActor()
{
	SnakeActor = GetWorld()->SpawnActor<ASnakeBase>(SnakeActorClass, FTransform());
	SnakeActor->PawnOwner = this;
	SnakeActor->ProceduralGrid->SetMaterial(0, Material_Grid);
}

void APlayerPawnBase::HandlePlayerVerticalInput(float value)
{
	if (IsValid(SnakeActor))
	{
		if (value > 0 && SnakeActor->SnakeRequestedMovementDirection!=EMovementDirection::DOWN && SnakeActor->TickEnded)
		{
			SnakeActor->SnakeRequestedMovementDirection = EMovementDirection::UP;
			SnakeActor->TickEnded = false;
		}
		else if (value < 0 && SnakeActor->SnakeRequestedMovementDirection != EMovementDirection::UP && SnakeActor->TickEnded)
		{
			SnakeActor->SnakeRequestedMovementDirection = EMovementDirection::DOWN;
			SnakeActor->TickEnded = false;
		}
	}
}

void APlayerPawnBase::HandlePlayerHorizontalInput(float value)
{
	if (IsValid(SnakeActor))
	{
		if (value > 0 && SnakeActor->SnakeRequestedMovementDirection != EMovementDirection::LEFT && SnakeActor->TickEnded)
		{
			SnakeActor->SnakeRequestedMovementDirection = EMovementDirection::RIGHT;
			SnakeActor->TickEnded = false;
		}
		else if (value < 0 && SnakeActor->SnakeRequestedMovementDirection != EMovementDirection::RIGHT && SnakeActor->TickEnded)
		{
			SnakeActor->SnakeRequestedMovementDirection = EMovementDirection::LEFT;
			SnakeActor->TickEnded = false;
		}
	}
}

void APlayerPawnBase::SpawnFood()
{
	float x = 0, y = 0;
	FVector RandomFoodLocation(0, 0, 0);
	FVector FoodScale(0.15, 0.15, 0.15);
	FTransform RandomFoodTransform;
	
	int iterator = 0;
	bool bSpawned = false;
	TArray<AActor*> ActorsToIgnore;
	TArray<FHitResult> HitResult;
	while (iterator < 150 && !bSpawned)
	{
		//���������� ����� ������
		
		RandomFoodLocation = FMath::VRand() * SnakeActor->SnakeMoveRadius;
//		RandomFoodLocation = FVector(1,0,0) * SnakeActor->SnakeMoveRadius;
		RandomFoodTransform = FTransform(RandomFoodLocation);
		
		/*x = (rand() % 16 - 8) * 60 + 20;
		y = (rand() % 35 - 17) * 60;
		RandomFoodLocation = FVector(x, y, 0);
		RandomFoodTransform = FTransform(RandomFoodLocation);*/


		UKismetSystemLibrary::SphereTraceMulti(GetWorld(), RandomFoodLocation, RandomFoodLocation, SnakeActor->ElementSize / 2,
										   	   TraceTypeQuery1, 0, ActorsToIgnore, EDrawDebugTrace::ForDuration, HitResult,
											   1, FLinearColor::Green, FLinearColor::Red, 5);
		if (HitResult.Num() == 0)
		{
			int32 RandomBonus = rand() % 100;
			if ((RandomBonus > 74) && (RandomBonus < 95))
			{
				FoodActorClass = AFood_MoreBlocks::StaticClass();
				Material_Food = Material_Food_MoreBlocks;
			}
			else if (RandomBonus > 94)
			{
				FoodActorClass = AFood_IncreasedSpeed::StaticClass();
				Material_Food = Material_Food_Increased_Speed;
			}
			else if (RandomBonus < 75)
			{
				FoodActorClass = AFood::StaticClass();
				Material_Food = Material_Food_default;
			}
			FoodActor = GetWorld()->SpawnActor<AFood>(FoodActorClass, RandomFoodTransform);
			FoodActors.Add(FoodActor);
			FoodActor->MeshComponent->SetStaticMesh(Mesh_Food);
			FoodActor->MeshComponent->SetWorldScale3D(FoodScale);
			FoodActor->MeshComponent->SetMaterial(0, Material_Food);
			bSpawned = true;
		}
		iterator++;
	}
}

void APlayerPawnBase::SpawnWalls(int16 Amount)
{
	float x = 0, y = 0;
	FVector RandomWallLocation(0, 0, 0);
	FVector WallScale(0.5, 0.5, 0.5);
	FRotator WallRotator;
	FTransform RandomWallTransform;

	int iterator = 0;
	bool bSpawned = false;
	TArray<AActor*> ActorsToIgnore;
	TArray<FHitResult> HitResult;
	for (int i = 0; i < Amount; i++)
	{
		bSpawned = false;
		while (iterator < 150 && !bSpawned)
		{
			//���������� ����� ������

			RandomWallLocation = FMath::VRand() * SnakeActor->SnakeMoveRadius;
			//		RandomFoodLocation = FVector(1,0,0) * SnakeActor->SnakeMoveRadius;
			WallRotator = RandomWallLocation.Rotation();
			RandomWallTransform = FTransform(WallRotator, RandomWallLocation);

			/*x = (rand() % 16 - 8) * 60 + 20;
			y = (rand() % 35 - 17) * 60;
			RandomFoodLocation = FVector(x, y, 0);
			RandomFoodTransform = FTransform(RandomFoodLocation);*/


			UKismetSystemLibrary::SphereTraceMulti(GetWorld(), RandomWallLocation, RandomWallLocation, SnakeActor->ElementSize / 2,
				TraceTypeQuery1, 0, ActorsToIgnore, EDrawDebugTrace::ForDuration, HitResult,
				1, FLinearColor::Green, FLinearColor::Red, 5);
			if (HitResult.Num() == 0)
			{
				AWall* WallActor = GetWorld()->SpawnActor<AWall>(WallActorClass, RandomWallTransform);
				WallActor->GetRootComponent()->SetWorldScale3D(WallScale);
				WallActors.Add(WallActor);
				bSpawned = true;
			}
			iterator++;
		}
	}
}

void APlayerPawnBase::SetCameraOrthoWidth()
{
	FVector2D ViewportSize(ForceInitToZero);
	/*if(GEngine)
	{
		if (GEngine->GameViewport)
		{
			GEngine->GameViewport->GetViewportSize(ViewportSize);
			MainCameraHUD->ViewportIsValid = "Viewport Is Valid";
			MainCameraHUD->ViewportSizeHUD = ViewportSize.ToString();
		}
		else
		{
			MainCameraHUD->ViewportIsValid = "Viewport Is Invalid";
			MainCameraHUD->ViewportSizeHUD = "---";
		}
	}*/
	
	MainCameraHUD->OrthoWidthBefore = FString::SanitizeFloat(PawnCamera->OrthoWidth);
	GEngine->GameViewport->GetViewportSize(ViewportSize);
	//GEngine->GameUserSettings()->GetLastConfirmedScreenResolution();
	//UGameUserSettings* Settings = GEngine->GameUserSettings;
	//uint32 SizeX = GEngine->GetGameUserSettings()->
	float ViewportRatio, CameraOrthoWidth, LevelRatio;
	ViewportRatio = ViewportSize.Y / ViewportSize.X;
	CameraOrthoWidth = 2350;
	LevelRatio = static_cast<float>(1200) / 2350;
	if (ViewportRatio >= LevelRatio)
	{
		CameraOrthoWidth = 2350;
	}
	else
	{
		CameraOrthoWidth = 1200 * pow(ViewportRatio, -1);
	}
	PawnCamera->SetOrthoWidth(CameraOrthoWidth);
	MainCameraHUD->OrthoWidthAfter = FString::SanitizeFloat(PawnCamera->OrthoWidth);
}

void APlayerPawnBase::CreateSphereLevel()
{
	SphereLevelActor = GetWorld()->SpawnActor<ASphereLevel>(SphereLevelClass, FTransform());
	SphereLevelActor->PawnOwner = this;
}


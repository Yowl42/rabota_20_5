// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/SaveGame.h"
#include "SnakeGameScoreSave.generated.h"

/**
 * 
 */
UCLASS()
class SNAKEGAME_API USnakeGameScoreSave : public USaveGame
{
	GENERATED_BODY()

public:
	UPROPERTY(BlueprintReadOnly)
	int32 PlayerRecordScore = 0;

};

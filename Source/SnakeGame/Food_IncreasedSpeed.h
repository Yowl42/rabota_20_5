// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Food.h"
#include "Food_IncreasedSpeed.generated.h"

/**
 * 
 */
UCLASS()
class SNAKEGAME_API AFood_IncreasedSpeed : public AFood
{
	GENERATED_BODY()
	void Interact(AActor* Interactor, bool bIsHead) override;
};
